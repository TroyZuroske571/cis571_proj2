# multiAgents.py
# --------------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

from util import manhattanDistance
from game import Directions
import random, util

from game import Agent

class ReflexAgent(Agent):
  """
    A reflex agent chooses an action at each choice point by examining
    its alternatives via a state evaluation function.

    The code below is provided as a guide.  You are welcome to change
    it in any way you see fit, so long as you don't touch our method
    headers.
  """


  def getAction(self, gameState):
    """
    You do not need to change this method, but you're welcome to.

    getAction chooses among the best options according to the evaluation function.

    Just like in the previous project, getAction takes a GameState and returns
    some Directions.X for some X in the set {North, South, West, East, Stop}
    """
    # Collect legal moves and successor states
    legalMoves = gameState.getLegalActions()

    # Choose one of the best actions
    scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
    bestScore = max(scores)
    bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
    chosenIndex = random.choice(bestIndices) # Pick randomly among the best

    "Add more of your code here if you want to"

    return legalMoves[chosenIndex]

  def evaluationFunction(self, currentGameState, action):
    """
    Design a better evaluation function here.

    The evaluation function takes in the current and proposed successor
    GameStates (pacman.py) and returns a number, where higher numbers are better.

    The code below extracts some useful information from the state, like the
    remaining food (newFood) and Pacman position after moving (newPos).
    newScaredTimes holds the number of moves that each ghost will remain
    scared because of Pacman having eaten a power pellet.

    Print out these variables to see what you're getting, then combine them
    to create a masterful evaluation function.
    """
    # Useful information you can extract from a GameState (pacman.py)
    successorGameState = currentGameState.generatePacmanSuccessor(action)
    newFood = successorGameState.getFood()

    "*** YOUR CODE HERE ***"

    #print(newPos)
    #print(newFood)
    #print(newGhostStates)
    #print(newScaredTimes)

    result = successorGameState.getScore()
    #Get total manhattan distance of food and get penalty for next nearest food
    foodDist = getFood(self, currentGameState, action)

    #total manhattan distance
    distToFoodScore = foodDist[0]

    #negative penalty for next food
    nextFoodScore = - foodDist[1]

    #multiply to get score and store
    result = distToFoodScore * nextFoodScore

    #Get total manhattan distance of Ghost
    ghostScore = getGhosts(self, currentGameState, action)

    #If a ghost is right next to us move away immediately
    if ghostScore[1] >= 1:
        return ghostScore[0]
    else: #else divide the distances of ghosts by the raw food score
        ghostToFoodScore = (result / ghostScore[0])

    #If food is right next to us eat it immeditately because ghost are not next
    #to us at this point
    if foodDist[0] <= 1:
        result = float('inf')
        return result

    #Else return the food ghost score because food and ghost are not near us
    result = (ghostToFoodScore)

    return result

def getFood(self, currentGameState, action):
    successorGameState = currentGameState.generatePacmanSuccessor(action)
    newFood = currentGameState.getFood()
    newPos = successorGameState.getPacmanPosition()
    dist = 0
    nextFood = float('inf')
    for food in newFood.asList():
        dist += manhattanDistance(food, newPos)
        if (dist < nextFood):
            nextFood = dist

    return dist, nextFood

def getGhosts(self, currentGameState, action):
    successorGameState = currentGameState.generatePacmanSuccessor(action)
    newPos = successorGameState.getPacmanPosition()
    newGhostStates = successorGameState.getGhostStates()
    ghostPos = [ghost.getPosition() for ghost in newGhostStates]
    ghostDis = 0.0
    for ghost in ghostPos:
        ghostDis += ghostDis + float(manhattanDistance(newPos, ghost))

    # Ghost is right next to us run away (using 3 steps to be safe)
    if ghostDis < 3:
        result = -float('inf')
        return (result, 1)

    return (((ghostDis)), 0)

def scoreEvaluationFunction(currentGameState):
  """
    This default evaluation function just returns the score of the state.
    The score is the same one displayed in the Pacman GUI.

    This evaluation function is meant for use with adversarial search agents
    (not reflex agents).
  """
  return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
  """
    This class provides some common elements to all of your
    multi-agent searchers.  Any methods defined here will be available
    to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

    You *do not* need to make any changes here, but you can if you want to
    add functionality to all your adversarial search agents.  Please do not
    remove anything, however.

    Note: this is an abstract class: one that should not be instantiated.  It's
    only partially specified, and designed to be extended.  Agent (game.py)
    is another abstract class.
  """

  def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
    self.index = 0 # Pacman is always agent index 0
    self.evaluationFunction = util.lookup(evalFn, globals())
    self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
  """
    Your minimax agent (question 2)
  """

  def getAction(self, gameState):
    """
      Returns the minimax action from the current gameState using self.depth
      and self.evaluationFunction.

      Here are some method calls that might be useful when implementing minimax.

      gameState.getLegalActions(agentIndex):
        Returns a list of legal moves for an agent
        agentIndex=0 means Pacman, ghosts are >= 1

      Directions.STOP:
        The stop direction, which is always legal

      gameState.generateSuccessor(agentIndex, action):
        Returns the successor game state after an agent takes an action

      gameState.getNumAgents():
        Returns the total number of agents in the game
    """
    "*** YOUR CODE HERE ***"
    #Used pseduocode from AI book found on page 166 to implement

    # Function MINIMAX-DECISION from book used to select the best action except
    # agent included for pac man or ghost and depth needed as well
    def minimaxDecision (gameState, agent, depth):
        # Get all moves
        legalMove = gameState.getLegalActions(0)
        # remove Directions.STOP like instructions state
        legalMove.remove(Directions.STOP)
        v = float("-inf")
        for move in legalMove:
            state = gameState.generateSuccessor(0, move)
            temp = minValue(state, agent, self.depth)
            if temp > v:
                v = temp
                action = move
        return action


    # MAX-VALUE function from book except depth needed as well
    def maxValue(state, depth):
          # TERMINAL-TEST
          if state.isWin() or state.isLose() or depth == 0:
              # return UTILITY function
              return self.evaluationFunction(state)
          else:
              v = float("-inf")
              agent = 1 #pacman agent
              legalMoves = state.getLegalActions(0)
              successorStates = []
              for move in legalMoves:
                  successorStates.append(state.generateSuccessor(0, move))
              for states in successorStates:
                  v = max(v, minValue(states, agent, depth))
              return v

    # MIN-VALUE function from book except agent and depth included
    def minValue(state, agent, depth):
          #TERMINAL-TEST
          if state.isWin() or state.isLose() or depth == 0:
              #return UTILITY function
              return self.evaluationFunction(state)
          else:
              v = float("inf")
              legalMoves = state.getLegalActions(agent)
              successorStates = []
              for move in legalMoves:
                  successorStates.append(state.generateSuccessor(agent, move))
              for states in successorStates:
                  if agent != state.getNumAgents() - 1:
                      v = min(v, minValue(states, agent + 1,
                                               depth))  # agent now a ghost
                  else:
                      v = min(v, maxValue(states, depth - 1))
              return v


    return minimaxDecision(gameState, 1, self.depth)

class AlphaBetaAgent(MultiAgentSearchAgent):
  """
    Your minimax agent with alpha-beta pruning (question 3)
  """

  def getAction(self, gameState):
    """
      Returns the minimax action using self.depth and self.evaluationFunction
    """
    "*** YOUR CODE HERE ***"
    # Used pseduocode from AI book found on page 170 to implement

    # Function alphaBetaSearchfrom book used to select the best action except
    # agent included for pac man or ghost and depth needed as well
    def alphaBetaSearch(gameState, agent, depth):
            # Get all moves
            legalMove = gameState.getLegalActions(0)

            # remove Directions.STOP like instructions state
            legalMove.remove(Directions.STOP)
            v = float("-inf")
            alpha = float("-inf")
            beta = float("inf")
            agent = 1  # pacman agent
            for move in legalMove:
                state = gameState.generateSuccessor(0, move)
                temp = minValue(state, agent, self.depth, alpha, beta)
                if temp > v:
                    v = temp
                    action = move
            return action

    # MAX-VALUE function from book except agent included for pac man or ghost and
    # depth needed as well

    def maxValue(state, depth, alpha, beta):
          # TERMINAL-TEST
          if state.isWin() or state.isLose() or depth == 0:
              # return UTILITY function
              return self.evaluationFunction(state)
          else:
              v = float("-inf")
              agent = 1  # pacman agent
              legalMoves = state.getLegalActions(0)
              successorStates = []
              for move in legalMoves:
                  successorStates.append(state.generateSuccessor(0, move))
              for states in successorStates:
                  v = max(v, minValue(states, agent, depth, alpha, beta))

              if v >= beta:
                  return v
              alpha = max(alpha, v)
              return v

  # MIN-VALUE function from book except agent and depth included
    def minValue(state, agent, depth, alpha, beta):
          # TERMINAL-TEST
          if state.isWin() or state.isLose() or depth == 0:
              # return UTILITY function
              return self.evaluationFunction(state)
          else:
              v = float("inf")
              legalMoves = state.getLegalActions(agent)
              successorStates = []
              for move in legalMoves:
                  successorStates.append(state.generateSuccessor(agent, move))
              for states in successorStates:
                  if agent != state.getNumAgents() - 1:
                      v = min(v, minValue(states, agent + 1, depth, alpha,
                                          beta))  # agent now a ghost
                  else:
                      v = min(v, maxValue(states, depth - 1, alpha, beta))
              if v <= alpha:
                  return v
              beta = min(beta, v)
              return v

    return alphaBetaSearch(gameState, 1, self.depth)

class ExpectimaxAgent(MultiAgentSearchAgent):
  """
    Your expectimax agent (question 4)
  """

  def getAction(self, gameState):
    """
      Returns the expectimax action using self.depth and self.evaluationFunction

      All ghosts should be modeled as choosing uniformly at random from their
      legal moves.
    """
    "*** YOUR CODE HERE ***"
    # Used pseduocode from AI book found on page 178 to implement

    # Function EXPECTMINIMAX from book used to select the best action
    def expectMiniMax(gameState, agent, depth):
        # Get all moves
        legalMove = gameState.getLegalActions(0)

        # remove Directions.STOP like instructions state
        legalMove.remove(Directions.STOP)
        v = float("-inf")
        agent = 1  # pacman agent
        for move in legalMove:
            state = gameState.generateSuccessor(0, move)
            temp = minValue(state, agent, self.depth)
            if temp > v:
                v = temp
                action = move
        return action


    # MAX-VALUE function from book except agent included for pac man or ghost
    # and depth needed as well

    def maxValue(state, depth, agent):
          # TERMINAL-TEST
          if state.isWin() or state.isLose() or depth == 0:
              # return UTILITY function
              return self.evaluationFunction(state)
          else:
              legalMoves = state.getLegalActions(0)
              if len(legalMoves) > 0:
                  v = 0
              else:
                  # Pacman perceives he could be trapped but might escape to grab a
                  # few more pieces of food
                  v = self.evaluationFunction(state)
              for move in legalMoves:
                  # no longer take the min over all ghost actions
                  eval = minValue(state.generateSuccessor(agent, move),
                                       agent + 1, depth)
                  if eval > v:
                      v = eval

              return v


  # MIN-VALUE function from book except agent and depth included
    def minValue(state, agent, depth):
          # TERMINAL-TEST
          if state.isWin() or state.isLose() or depth == 0:
              # return UTILITY function
              return self.evaluationFunction(state)
          else:
              v = 0
              legalMoves = state.getLegalActions(agent)
              for move in legalMoves:
                  if agent != state.getNumAgents() - 1:
                      # no longer take the min over all ghost actions
                      v += minValue(state.generateSuccessor(agent, move),
                                         agent + 1, depth) # agent now a ghost
                  else:
                      #no longer take the min over all ghost actions
                      v += maxValue(state.generateSuccessor(agent, move),
                                    depth + 1, 0) #agent pac man
              if len(legalMoves) > 0:
                  v /= len(legalMoves)
              else:
                  # Pacman perceives he could be trapped but might escape to
                  # grab a few more pieces of food
                  v = self.evaluationFunction(state)

              return v

    return expectMiniMax(gameState, 1, self.depth)

def betterEvaluationFunction(currentGameState):
  """
    Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
    evaluation function (question 5).

    DESCRIPTION: <write something here so we know what you did>
  """
  "*** YOUR CODE HERE ***"
  util.raiseNotDefined()

# Abbreviation
better = betterEvaluationFunction

class ContestAgent(MultiAgentSearchAgent):
  """
    Your agent for the mini-contest
  """

  def getAction(self, gameState):
    """
      Returns an action.  You can use any method you want and search to any depth you want.
      Just remember that the mini-contest is timed, so you have to trade off speed and computation.

      Ghosts don't behave randomly anymore, but they aren't perfect either -- they'll usually
      just make a beeline straight towards Pacman (or away from him if they're scared!)
    """
    "*** YOUR CODE HERE ***"
    util.raiseNotDefined()
